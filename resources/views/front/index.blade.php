@extends('layout')

@section('content')

	<section id="top">
		<div class="container">
			<div class="row">
				<div class="col-xl-12 text-center">
					<a href="https://ucg.edu.mx" target="_blank">
						<img src="https://ucg.edu.mx/assets/img/logo.svg" class="img-fluid logo" alt="Universidad Cuauhtémoc">
					</a>
				</div>
			</div>
		</div>
	</section>

	<section id="body">
		<div class="container">
			<div class="row">
				<div class="col-xl-6 offset-xl-3">
					{{-- <h1>Jornadas de <span>Conferencias</span></h1> --}}
					<img src="{{asset('/img/agosto/titulo.png')}}" class="img-fluid" alt="Jornadas 2020 de conferencias">
					<img src="{{asset('/img/zoom-blue.png')}}" class="img-fluid zoom" alt="Zoom">
					<div id="form">
						<form method="post" action="{{ route('front.save') }}">
							@csrf
							@if ($errors->any())
							    <div class="alert alert-danger">
							        <ul>
							            @foreach ($errors->all() as $error)
							                <li>{{ $error }}</li>
							            @endforeach
							        </ul>
							    </div>
						    @endif
						    @if (Session::has('msg-success'))
							    <div class="alert alert-success">
							        <ul>
							            <li>{!! Session::get('msg-success') !!}</li>
							        </ul>
							    </div>
							@endif
							<div class="row">
								<div class="col-xl-6">
									<div class="form-group">
										<label>Nombre:*</label>
										<input type="text" name="name" class="form-control @if ($errors->first('name')) error @endif" value="{{ old('name') }}" required>
									</div>
								</div>
								<div class="col-xl-6">
									<div class="form-group">
										<label>Teléfono:*</label>
										<input type="number" name="phone" class="form-control @if ($errors->first('phone')) error @endif" value="{{ old('phone') }}" required>
									</div>
								</div>
							</div>
							
							<div class="form-group">
								<label>Correo electrónico:*</label>
								<input type="email" name="email" class="form-control @if ($errors->first('email')) error @endif" value="{{ old('email') }}" required>
							</div>

							<div class="form-group">
								<label>Ocupación:*</label>
								<select class="form-control" name="ocupation" required>
									<option value="Estudiante UCG">Estudiante UCG</option>
								    <option value="Estudiante">Estudiante</option>
								    <option value="Profesionista">Profesionista</option>
								    <option value="Otro">Otro</option>
								</select>
							</div>

							<div class="form-group">
								<label>Conferencia:*</label>
								<select class="form-control" name="conference" required>
									<option value="5 de Agosto 1:00 pm. - Elaboración de Pepián Rojo Poblano">5 de Agosto - Elaboración de Pepián Rojo Poblano</option>
									<option value="6 de Agosto 10:00 am. - Manejo de vía aérea con Aerobox para pacientes con Covid">6 de Agosto - Manejo de vía aérea con Aerobox para pacientes con Covid</option>
									<option value="6 de Agosto 6:00 pm. - Sistema inmune en tiempos de COVID 19">6 de Agosto - Sistema inmune en tiempos de COVID 19</option>
									<option value="07 de Agosto 6:00pm. - Marketing Digital, Posicionamiento en redes">7 de Agosto - Marketing Digital, Posicionamiento en redes</option>
									<option value="11 de Agosto 12:00 pm. - Seguro de gastos medicos mayores / Fondo de ahorro / Retiro">11 de Agosto - Seguro de gastos medicos mayores  / Fondo de ahorro / Retiro</option>
									<option value="11 de Agosto 6:00pm. - Resilencia en contexto de la nueva normalidad">11 de Agosto - Resilencia en contexto de la nueva normalidad</option>
									<option value="18 de Agosto 6:00pm. - Psicología deportiva y la competencia en tiempos de pandemia">18 de Agosto - Psicología deportiva y la competencia en tiempos de pandemia</option>
									<option value="26 de Agosto 6:00pm. - Ruptura financiera y recuperación económica ante la nueva normalidad">26 de Agosto - Ruptura financiera y recuperación económica ante la nueva normalidad</option>
								</select>
							</div>

							<button type="submit" class="btn btn-send btn-block btn-lg">Apartar mi lugar</button>
					
						</form>
					</div>

					<div class="owl-carousel owl-theme">

						<div class="item">
							<div class="img" style="background-image: url('{{asset('img/agosto/00.png')}}');"></div>
						</div>
						<div class="item">
							<div class="img" style="background-image: url('{{asset('img/agosto/0.png')}}');"></div>
						</div>
						<div class="item">
							<div class="img" style="background-image: url('{{asset('img/agosto/1.png')}}');"></div>
						</div>
						<div class="item">
							<div class="img" style="background-image: url('{{asset('img/agosto/2.png')}}');"></div>
						</div>
						<div class="item">
							<div class="img" style="background-image: url('{{asset('img/agosto/3.png')}}');"></div>
						</div>
						<div class="item">
							<div class="img" style="background-image: url('{{asset('img/agosto/4.png')}}');"></div>
						</div>
						<div class="item">
							<div class="img" style="background-image: url('{{asset('img/agosto/5.png')}}');"></div>
						</div>
						<div class="item">
							<div class="img" style="background-image: url('{{asset('img/agosto/6.png')}}');"></div>
						</div>

					</div>

				</div>
			</div>
		</div>
	</section>

	<section id="footer">
		<div class="container">
			<div class="row">
				<div class="col-xl-12 text-center">
					<p>Copyright &copy; {{ date('Y') }} Universidad Cuauhtémoc - Todos los derechos reservados.</p>
				</div>
			</div>
		</div>
	</section>

@endsection