<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>@yield('name')</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="{{asset('css/app.css')}}">
        <link rel="stylesheet" type="text/css" href="{{asset('plugins/owl/assets/owl.carousel.min.css')}}">
        <link rel="stylesheet" type="text/css" href="{{asset('plugins/owl/assets/owl.theme.default.min.css')}}">
    </head>
    <body style="background-image: url('{{asset('img/fondo.png')}}');">
    	@yield('content')
    </body>
    <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
    <script src="{{asset('plugins/owl/owl.carousel.min.js')}}"></script>
    <script src="{{asset('js/main.js')}}"></script>
</html>
