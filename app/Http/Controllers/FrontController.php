<?php

namespace App\Http\Controllers;

use App\Lead;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\SendLead;
use App\Mail\SendInformation;
use App\Exports\LeadsExport;
use Maatwebsite\Excel\Facades\Excel;


class FrontController extends Controller
{
    public function index(){
		return view('front.index');
	}

	public function save(Request $request) {

		$rules = [
	        'name' => 'required',
			'phone' => 'required|digits:10',
			'email' => 'required|unique:leads',
			'ocupation' => 'required',
			'conference' => 'required',
	    ];

	    $customMessages = [
	        'required' => 'El campo es requerido.',
	        'phone.digits' => 'El teléfono requiere de 10 dígitos.',
	        'email.unique' => 'El correo ya fue registrado.',
	    ];

	    //Realizamos la validación
	    $this->validate($request, $rules, $customMessages);

	    //Guardamos el Lead en la base
	    $lead = Lead::create($request->all());

	    //Enviamos el correo al administrador.
	    $sends = array(
	    	'ucg@ucg.edu.mx',
	    	'vinculacion@ucg.edu.mx'
	    );

	    for ($i = 0; $i < count($sends); $i++) { 
	    	Mail::to($sends[$i])->send(new SendLead($lead));
	    }

	    //Enviamos el correo al usuario
	    Mail::to($lead->email)->send(new SendInformation($lead));

	    return redirect()->back()->with('msg-success','Se ha registrado correctamente, revisa tu correo.');
	}

	public function export(){
		return Excel::download(new LeadsExport, 'leads.xlsx');
	}
}


